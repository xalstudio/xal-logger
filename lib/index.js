"use strict";Object.defineProperty(exports,"__esModule",{value:true});exports.default=void 0;require("core-js/modules/es6.array.for-each");require("core-js/modules/es6.array.filter");require("core-js/modules/es6.object.keys");require("core-js/modules/es6.object.define-property");require("core-js/modules/es6.date.now");require("core-js/modules/es6.function.bind");require("core-js/modules/web.dom.iterable");require("core-js/modules/es6.array.iterator");require("core-js/modules/es7.object.values");require("core-js/modules/es7.array.includes");require("core-js/modules/es6.string.includes");var _util=require("util");function _objectSpread(target){for(var i=1;i<arguments.length;i++){var source=arguments[i]!=null?arguments[i]:{};var ownKeys=Object.keys(source);if(typeof Object.getOwnPropertySymbols==="function"){ownKeys=ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function(sym){return Object.getOwnPropertyDescriptor(source,sym).enumerable}))}ownKeys.forEach(function(key){_defineProperty(target,key,source[key])})}return target}function _classCallCheck(instance,Constructor){if(!(instance instanceof Constructor)){throw new TypeError("Cannot call a class as a function")}}function _defineProperties(target,props){for(var i=0;i<props.length;i++){var descriptor=props[i];descriptor.enumerable=descriptor.enumerable||false;descriptor.configurable=true;if("value"in descriptor)descriptor.writable=true;Object.defineProperty(target,descriptor.key,descriptor)}}function _createClass(Constructor,protoProps,staticProps){if(protoProps)_defineProperties(Constructor.prototype,protoProps);if(staticProps)_defineProperties(Constructor,staticProps);return Constructor}function _defineProperty(obj,key,value){if(key in obj){Object.defineProperty(obj,key,{value:value,enumerable:true,configurable:true,writable:true})}else{obj[key]=value}return obj}/**
 * @class
 * @author Alexandru Prisacariu <zalexxanderx@gmail.com>
 */var Logger=/*#__PURE__*/function(){/**
     * The types of logging
     * @memberof Logger
     * @enum {Number}
     * @readonly
     */ /**
     * @constructor
     * 
     * @param {Object} settings the settings of the logger
     * @param {String} settings.app the name of the app
     * @param {Number} [settings.instance=0] the instance number of the app
     * @param {Number} [settings.type=LogType.SIMPLE] the logging type @see {@link Logger.LogType}
     */function Logger(settings){_classCallCheck(this,Logger);if(!settings)throw new Error("The required parameter 'settings' is missing");else if(!settings.app)throw new Error("No valid app name was selected");else if(settings.type!=null&&!Object.values(Logger.LogType).includes(settings.type)){throw new Error("No valid log type was selected")}this.topFields={app:settings.app,instance:settings.instance||0};this.depth=settings.depth;if(settings.type==null||settings.type==Logger.LogType.SIMPLE){this._createLoggerLevel=this._createSimpleLoggerLevel.bind(this)}else if(settings.type==Logger.LogType.JSON){this._createLoggerLevel=this._createJSONLoggerLevel.bind(this)}this.createLogger=this.createLogger.bind(this)}/**
     * Create a logger function of type JSON
     * @private
     * @memberof Logger
     * 
     * @param {Function} output the function that will output the log
     * @param {String} type the level of the log
     * @param {String} group the name of the group
     * 
     * @returns {Function} the logging function
     */_createClass(Logger,[{key:"_createJSONLoggerLevel",value:function _createJSONLoggerLevel(output,type,group){var _this=this;return function(message){var data=arguments.length>1&&arguments[1]!==undefined?arguments[1]:null;output((0,_util.inspect)(_objectSpread({},_this.topFields,{group:group,message:message,type:type,timestamp:Date.now(),otherData:data}),{breakLength:Infinity,depth:_this.depth}))}}/**
     * Create a logger function of type SIMPLE
     * @private
     * @memberof Logger
     * 
     * @param {Function} output the function that will output the log
     * @param {String} type the level of the log
     * @param {String} group the name of the group
     * 
     * @returns {Function} the logging function
     */},{key:"_createSimpleLoggerLevel",value:function _createSimpleLoggerLevel(output,type,group){var _this2=this;return function(message){var data=arguments.length>1&&arguments[1]!==undefined?arguments[1]:null;output("[".concat(_this2.topFields.app,"][").concat(_this2.topFields.instance,"][").concat(Date.now(),"][").concat(group,"][").concat(type,"]").concat(message),data?data:undefined)}}/**
     * Create a logger object for a specific group
     * @public
     * @memberof Logger
     * 
     * @param {String} group the name of the group
     */},{key:"createLogger",value:function createLogger(group){return{log:this._createLoggerLevel(console.log,"LOG",group),warn:this._createLoggerLevel(console.log,"WARN",group),error:this._createLoggerLevel(console.error,"ERROR",group)}}}]);return Logger}();_defineProperty(Logger,"LogType",{SIMPLE:0,JSON:1});;var _default=Logger;exports.default=_default;