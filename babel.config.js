module.exports = {
    "presets": [
        [
            "@babel/env",
            {
                "useBuiltIns": "usage"
            }
        ]
    ],
    "plugins": [
        [
            "@babel/plugin-proposal-class-properties"
        ]
    ]
};