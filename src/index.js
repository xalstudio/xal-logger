import { inspect } from 'util'

/**
 * @class
 * @author Alexandru Prisacariu <zalexxanderx@gmail.com>
 */
class Logger {
    /**
     * The types of logging
     * @memberof Logger
     * @enum {Number}
     * @readonly
     */
    static LogType = {
        SIMPLE: 0,
        JSON: 1
    };

    /**
     * @constructor
     * 
     * @param {Object} settings the settings of the logger
     * @param {String} settings.app the name of the app
     * @param {Number} [settings.instance=0] the instance number of the app
     * @param {Number} [settings.type=LogType.SIMPLE] the logging type @see {@link Logger.LogType}
     */
    constructor(settings) {
        if (!settings) throw new Error("The required parameter 'settings' is missing");
        else if (!settings.app) throw new Error("No valid app name was selected");
        else if (settings.type != null && !Object.values(Logger.LogType).includes(settings.type)) {
            throw new Error("No valid log type was selected");
        }
        
        this.topFields = {
            app: settings.app,
            instance: settings.instance || 0
        }

        this.depth = settings.depth;

        if (settings.type == null || settings.type == Logger.LogType.SIMPLE) {
            this._createLoggerLevel = this._createSimpleLoggerLevel.bind(this);
        } else if (settings.type == Logger.LogType.JSON) {
            this._createLoggerLevel = this._createJSONLoggerLevel.bind(this);
        }

        this.createLogger = this.createLogger.bind(this);
    }

    /**
     * Create a logger function of type JSON
     * @private
     * @memberof Logger
     * 
     * @param {Function} output the function that will output the log
     * @param {String} type the level of the log
     * @param {String} group the name of the group
     * 
     * @returns {Function} the logging function
     */
    _createJSONLoggerLevel(output, type, group) {
        return (message, data = null) => {
            output(inspect({
                ...this.topFields,
                group: group,
                message: message,
                type: type,
                timestamp: Date.now(),
                otherData: data
            }, { breakLength: Infinity, depth: this.depth }))
        }
    }

    /**
     * Create a logger function of type SIMPLE
     * @private
     * @memberof Logger
     * 
     * @param {Function} output the function that will output the log
     * @param {String} type the level of the log
     * @param {String} group the name of the group
     * 
     * @returns {Function} the logging function
     */
    _createSimpleLoggerLevel(output, type, group) {
        return (message, data = null) => {
            output(`[${this.topFields.app}][${this.topFields.instance}][${Date.now()}][${group}][${type}]${message}`, data ? data : undefined);
        }
    }

    /**
     * Create a logger object for a specific group
     * @public
     * @memberof Logger
     * 
     * @param {String} group the name of the group
     */
    createLogger(group) {
        return {
            log: this._createLoggerLevel(console.log, "LOG", group),
            warn: this._createLoggerLevel(console.log, "WARN", group),
            error: this._createLoggerLevel(console.error, "ERROR", group)
        }
    }
};

export default Logger;